#! /usr/bin/env python3

import numpy as np
import tkinter as tk
import matplotlib.pyplot as plt
from statistics import mean
#import matplotlib.animation as anim

from matplotlib import style
style.use('ggplot')


#start weight: output = 0.5x + 0.5, learnng weight 0.3

#------------------------------------------------------------------------------------------------------------------

inputLayer=[0.3, 0.35, 0.4, 0.5, 0.6, 0.8, 0.95, 1.1]
targetOutput=[1.6, 1.4, 1.4, 1.6, 1.7, 2.0, 1.7, 2.1]
actuallOutput=[]

WINDOWWIDTH = 500
WINDOWHEIGHT = 300

perceptorWeight1 = 0.5
perceptorWeigth2 = 0.5
errorCheck = 1
#------------------------------------------------------------------------------------------------------------------
def end(event):
    global root
    root.destroy()

class NeuralNetwork (tk.Frame):
    def __init__(self, master=None):

        def Perceptron(input, target):

            global perceptorWeight1
            global perceptorWeigth2
            global errorCheck

            weight1 = perceptorWeight1
            weight2 = perceptorWeigth2

            output = (weight1 * input) + weight2

            error = target - output
            learningRate = 0.3

            weight1 = perceptorWeight1
            deltaWeight1 = learningRate * error * input
            weight1 = (weight1 + deltaWeight1)

            weight2 = perceptorWeigth2
            deltaWeight2 = learningRate * error * 1
            weight2 = (weight2 + deltaWeight2)

            perceptorWeight1 = weight1
            perceptorWeigth2 = weight2
            errorCheck = error

            actuallOutput.append(output)

            #print ("\n\nWeight 1:")
            #print (weight1)
            #print ("Weight 2:")
            #print (weight2)
            #print ("Output after training:")
            print (output)
            #print (error)
            #return (error)


        #sigmoid function
        #def nonlin(x, deriv=False):
        #    if(deriv==True):
        #        return x*(1-x)
        #    return 1/(1+np.exp(-x))

        #canvas to be drawn upon, x position, y position, radius
        def cicrcle(canvas, x, y, r):
            id = canvas.create_oval(x-r, y-r*(-1), x+r, y+r*(-1), fill="blue")
            return id

        def line(canvas, x1, y1, x2, y2):
            x1 = x1*xAxisScale
            y1 = y1*yAxisScale * 2.2
            y1 = window.winfo_reqheight() -y1     # invert Y-axis in order to get low values at the bottom om the screen

            x2 = x2*xAxisScale
            y2 = y2*yAxisScale
            y2 = window.winfo_reqheight() -y2     # invert Y-axis in order to get low values at the bottom om the screen

            line = canvas.create_line(x1, y1, x2, y2, fill='red')

        def line2(canvas, w1, w2):
            x1 = xAxisScale * inputLayer[0]
            y1 = (WINDOWHEIGHT/2.7) * (inputLayer[0] * w1 + 1*w2)
            x2 = xAxisScale * (inputLayer[len(inputLayer)-1])
            y2 = (WINDOWHEIGHT/2.7) * (inputLayer[len(inputLayer)-1])*w1 + 1*w2

            canvas.create_line(x1, y1, x2, y2)


        #def bestFitSlope (x, y):
        #    x = np.array(x)
        #    y = np.array(y)
        #    x = x * xAxisScale
        #    y = y * yAxisScale
        #    y = window.winfo_reqheight() -y     # invert Y-axis in order to get low values at the bottom om the screen

        #    line = (((mean(x)*mean(y)) - mean(x*y)) / ((mean(x)*mean(x)) - mean(x*x)))

        #    b = mean(y) - line*mean(x)

        #    return line, b

        tk.Frame.__init__(self, master)
        self.master=master
        master.title("Neural network task")

        self.label = tk.Label(master, text="A Perceptron learning exercise")
        self.label.pack()

        self.pack()
        window = tk.Canvas(master, width=WINDOWWIDTH, height=WINDOWHEIGHT)
        window.pack()

        xAxisScale = (WINDOWWIDTH*3/4)
        yAxisScale = (WINDOWHEIGHT/3)

        tk.oval={}
        #while errorCheck > 0.1:
        for targets in range(8):
            Perceptron (inputLayer[targets], targetOutput[targets])

            #draw points in graph for input (x) target (y) values
            x = inputLayer[targets]*xAxisScale
            y = targetOutput[targets]*yAxisScale
            y = window.winfo_reqheight() -y     # invert Y-axis in order to get low values at the bottom om the screen

            cicrcle(window, x, y, 5)
            #print (err)

            #print(errorCheck)
        line(window, min(inputLayer), min(actuallOutput), max(inputLayer), max(actuallOutput))
        #line2(window, perceptorWeight1, perceptorWeigth2)

        #line, b = bestFitSlope(inputLayer, actuallOutput)
        #print (line, b)

        #regression_line = []
        #for x in inputLayer:
        #    regression_line.append((line*x)+b)
        #print(regression_line)

        #for targets in range(8):
            #print(actuallOutput[targets])
        window.pack()


#if run as a program
#if __name__=="__main__":
root=tk.Tk()
#    Example(root).pack(fill="both", expand="True")
app = NeuralNetwork(master=root)
root.bind("<Escape>", end)
#root.bind("<Return", app.Perceptron)
root.mainloop()
